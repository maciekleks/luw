# build stage
FROM golang:stretch AS build-env
RUN mkdir -p /go/src/luw
WORKDIR /go/src/luw
COPY . .
RUN groupadd -g 20001 webhook && \
	useradd -u 10001 -g webhook webhook
RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o luw

FROM scratch
COPY --from=build-env /go/src/luw .
COPY --from=build-env /etc/passwd /etc/passwd
USER webhook
ENTRYPOINT ["/luw"]