package main

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/golang/glog"
)

const (
	defaultPort = "8443"
)

var (
	tlscert, tlskey string
)

func init() {
	flag.StringVar(&tlscert, "tlsCertFile", "/etc/certs/cert.pem", "File containing the Webhook x509 Certificate in PEM format.")
	flag.StringVar(&tlskey, "tlsKeyFile", "/etc/certs/key.pem", "File containing the Webhook Private Key in PEM format.")
	flag.Parse()
}

func main() {
	glog.Infof("Args: %+v\n", os.Args)

	port := os.Getenv("PORT")
	if len(port) == 0 {
		port = defaultPort
	}

	cert, err := tls.LoadX509KeyPair(tlscert, tlskey)
	if err != nil {
		glog.Errorf("Filed to load key pair: %v", err)
	}

	router := http.NewServeMux()
	router.HandleFunc("/log", serve())

	server := &http.Server{
		Addr: fmt.Sprintf(":%v", port),
		TLSConfig: &tls.Config{
			Certificates: []tls.Certificate{cert},
		},
		Handler: router,
	}

	go func() {
		if err := server.ListenAndServeTLS("", ""); err != nil {
			glog.Errorf("Failed to listen and serve webhook server: %v", err)
		}
	}()

	glog.Infof("Server running listening in port: %s", port)

	// listening shutdown singals
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)
	<-sigCh

	glog.Info("Got shutdown signal, shutting down webhook server gracefully...")
	server.Shutdown(context.Background())
}
