package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/golang/glog"
	admissionV1 "k8s.io/api/admission/v1"
)

func serve() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var body []byte
		if r.Body != nil {
			if data, err := ioutil.ReadAll(r.Body); err == nil {
				body = data
			}
		}
		if len(body) == 0 {
			glog.Error("empty body")
			http.Error(w, "empty body", http.StatusBadRequest)
			return
		}

		if r.URL.Path != "/log" {
			glog.Error("no validate")
			http.Error(w, "no validate", http.StatusBadRequest)
			return
		}

		arRequest := admissionV1.AdmissionReview{}
		glog.Infof("Request body: %s\n", string(body))
		if err := json.Unmarshal(body, &arRequest); err != nil {
			glog.Error("incorrect body")
			http.Error(w, "incorrect body", http.StatusBadRequest)
			return
		}

		glog.Infof("userInfo: %+v\n", arRequest.Request.UserInfo)
	}
}
